<?php

use App\Events\MessageNotification;
use App\Http\Controllers\API\CuentaController;
use App\Http\Controllers\API\PedidosController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('cuentas', CuentaController::class);
Route::apiResource('pedidos', PedidosController::class);

Route::get('broad', function(){
    event(new MessageNotification('this is a first message'));
});

Route::get('/listen', function(){
    return view(('listen'));
});
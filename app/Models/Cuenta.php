<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Notifications\Notifiable;

class Cuenta extends Model
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'nombre',
        'email',
        'telefono',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}

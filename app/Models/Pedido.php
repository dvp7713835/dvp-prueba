<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Pedido extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_cuenta',
        'producto',
        'cantidad',
        'valor',
        'total',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}

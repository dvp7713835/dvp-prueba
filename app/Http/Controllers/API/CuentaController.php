<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\GuardarCuentaRequest;
use App\Http\Resources\CuentaResource;
use App\Models\Cuenta;

class CuentaController extends Controller
{
    public function index()
    {
        return  CuentaResource::collection(Cuenta::all());
    }
    public function store(GuardarCuentaRequest $request)
    {
        return (new CuentaResource( Cuenta::create($request->all())))
        ->additional(['msg'=>'Account created']);;
    }
    public function show(Cuenta $cuenta)
    {
        return new CuentaResource($cuenta);
    }
    public function update(GuardarCuentaRequest $request, Cuenta $cuenta)
    {
        $cuenta->update($request->all());
        return (new CuentaResource($cuenta))
                ->additional(['msg'=>'Account updated'])
                ->response()
                ->setStatusCode(202);
    }
    public function destroy(Cuenta $cuenta)
    {
        $cuenta->delete();
        return (new CuentaResource($cuenta))->additional(['msg'=>'Account deleted']);
    }
}
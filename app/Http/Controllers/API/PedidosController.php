<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\GuardarPedidoRequest;
use App\Http\Requests\UpdatePedidoRequest;
use App\Http\Resources\PedidoResource;
use App\Models\Pedido;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PedidosController extends Controller
{
    public function index()
    {
        $result = DB::table('cuentas')
                ->join('pedidos', 'cuentas.id', '=', 'pedidos.id_cuenta')
                ->get();

        return response()->json([
            'response' => true,
            'data'     => $result,
        ],200);
    }
    public function store(GuardarPedidoRequest $request)
    {
        return(new PedidoResource(Pedido::create($request->all())))
        ->additional(['msg'=>'Pedido agregado']);;
    }
    public function show(Pedido $pedido)
    {
        return new PedidoResource($pedido);
    }
    public function update(UpdatePedidoRequest $request, Pedido $pedido)
    {
        $pedido->update($request->all());
        return(new PedidoResource($pedido))
        ->additional(['msg'=>'Pedido Actualizado'])
        ->response()->setStatusCode(202);
    }
    public function destroy(Pedido $pedido)
    {
        $pedido->delete();
        return (new PedidoResource($pedido))
        ->additional(['msg'=>'Pedido eliminado']);
    }
}